<?php

namespace app\models;

use Yii;
use Faker\Provider\Color;
use PHPUnit\Util\Log\JSON;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "products".
 *
 * @property int            $id
 * @property string         $path
 * @property string         $base_url
 * @property string         $title_ru
 * @property string         $title_en
 * @property string         $content_ru
 * @property string         $content_en
 * @property int            $price
 * @property int            $discount
 * @property int            $discount_price
 * @property int            $category_id
 * @property int            $color_id
 * @property int            $type_id
 * @property int            $status
 * @property int            $created_at
 * @property int            $updated_at
 *
 * @property SubCategory      $category
 * @property UserFavorites[]  $userFavorites
 * @property ProductImages[]  $productImages
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $attachments;    // Multiple Images      
    public $file;           // File  
    public $category;       // Category
    public $colors;         // Multiple Colors
    public $id_color;       // Colors() => ID
    public $types;          // Multiples Types 
    public $id_type;        // Types() => ID 
    public $buy_counts;     // Multiple  
    public $id_count;       // Count ID


   
    public static function tableName()
    {
        return 'products';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'productImages',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => false,
            ],

        ];
    }

   /**
     * @var $color_id, $type_id, buy_count
     * Encode to Json
     */ 
   public function beforeSave($insert)
   {
       $this->color_id = \yii\helpers\Json::encode(array_values($this->id_color));
       
       $this->type_id = \yii\helpers\Json::encode(array_values($this->id_type));
       
       $this->buy_count = \yii\helpers\Json::encode(array_values($this->id_count));

       return parent::beforeSave($insert);
   }


   /**
     * @var $colors, $types, $by_counts
     * Decode from Json
     */ 
   public function afterFind()
   {
       $color = \yii\helpers\Json::decode($this->color_id);
       $colors = Colors::findAll($color);

       if(!empty($colors)){
           $k = 0;
           foreach ($colors as $item){
               $this->colors[$k]['id_color'] = $item->id;
           $k++;
           }
       }

       $type = \yii\helpers\Json::decode($this->type_id);
       $types = Types::findAll($type);
       if(!empty($types)){
           $j = 0;
           foreach ($types as $item){
               $this->types[$j]['id_type'] = $item->id;
               $j++;
           }
       }

       $count = \yii\helpers\Json::decode($this->buy_count);
       if(!empty($count)){
           $i = 0;
           foreach ($count as $item){
               $this->buy_counts[$i]['id_count'] = $item;
               $i++;
           }
       }

       parent::afterFind(); 
   }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_en','title_ru','category_id','price'],'required'],
            [['content_ru', 'content_en','buy_count'], 'string'],
            [['price', 'discount', 'discount_price', 'category_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['path', 'base_url', 'title_ru', 'title_en','color_id', 'type_id'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['attachments', 'file','colors','types','id_color','id_type', 'buy_counts','id_count'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('app', 'ID'),
            'path'          => Yii::t('app', 'Image'),
            'base_url'      => Yii::t('app', 'Base Url'),
            'title_ru'      => Yii::t('app', 'Title Ru'),
            'title_en'      => Yii::t('app', 'Title En'),
            'content_ru'    => Yii::t('app', 'Content Ru'),
            'content_en'    => Yii::t('app', 'Content En'),
            'price'         => Yii::t('app', 'Price'),
            'discount'      => Yii::t('app', 'Discount'),
            'discount_price'=> Yii::t('app', 'Discount Price'),
            'category_id'   => Yii::t('app', 'Category ID'),
            'color_id'      => Yii::t('app', 'Colors'),
            'type_id'       => Yii::t('app', 'Types'),
            'buy_count'     => Yii::t('app', 'Buy counts'),
            'status'        => Yii::t('app', 'Available'),
            'created_at'    => Yii::t('app', 'Created At'),
            'updated_at'    => Yii::t('app', 'Updated At'),
            'attachments'   => Yii::t('app', 'Images'),
            'file'          => Yii::t('app', 'Main image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCategory()
    {
        return $this->hasOne(SubCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFavorites()
    {
        return $this->hasMany(UserFavorites::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    /**
     * @return $this->title_ru or $this->title_uz
     */
    public function getTitle(){

       return $this->{'title_'.Yii::$app->language} ;
    }

    /**
     * @return $this->content_ru or $this->content_uz
     */
    public function getContent(){
          return $this->{'content_'.Yii::$app->language} ;
    }

    /**
     * Добовление продукта в $_SESSION['cart']
     * Если продукт уже добавлен возврашаеть false
     * @param $product
     * @param $color
     * @param $type
     * @param $qty
     * @return bool
     */
    public function addToCart($product, $color, $type, $qty)
    {

        if ($product->discount_price == 0) {
            $price = $product->price;
        } else {
            $price = $product->discount_price;
        }

        $session = Yii::$app->session['cart'];

        if(!empty($session)){           
             $session[] = [
                'id'    => $product->id,
                'qty'   => $qty,
                'color' => $color->title,
                'type'  => $type->title,
                'name'  => $product->title,
                'price' => $price,
                'img'   => $product->path,
            ];          
        } else {
            return false;
        }

        Yii::$app->session['cart.qty'] = isset(Yii::$app->session['cart.qty']) ? Yii::$app->session['cart.qty'] + $qty : $qty;
        Yii::$app->session['cart.sum'] = isset(Yii::$app->session['cart.sum']) ? Yii::$app->session['cart.sum'] + $qty * $price : $qty * $price;
        return true;
    }
   
    /**
     * Перерасчет $_SESSION['cart'], $_SESSION['cart.qty'] и $_SESSION['cart.sum']
     *
     * @param $product_id
     * @param $color
     * @param $type
     * @return bool
     */
    public function recalc($product_id, $color, $type)
    {
        $session = Yii::$app->session['cart'];
        foreach ($session as $key => $item){
            if(($item['id'] == $product_id) && ($item['color'] == $color) && ($item['type'] == $type)){
                
                $price      = $item['price'];
                $qty        = $item['qty'];

                Yii::$app->session['cart.qty'] -= $qty;
                Yii::$app->session['cart.sum'] -= $qty * $price;

                unset($session[$key]);

            }
        }
        return false;

    }

}
