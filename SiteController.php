<?php
/**
 * User: NazirovLix
 * Date: 13.08.2018
 * Time: 17:58
 */

namespace app\controllers;

use app\models\AboutUsAgancy;
use app\models\AboutUsLeadership;
use app\models\AboutUsStructure;
use app\models\Album;
use app\models\AlbumImages;
use app\models\Feedback;
use app\models\forms\Login;
use app\models\Home;
use app\models\News;
use app\models\NewsComment;
use app\models\Video;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use Yii;

class SiteController extends Controller
{

    public function init()
    {

        // Check language from Cookies     
        if (!empty(Yii::$app->request->cookies['language'])) {
            Yii::$app->language = Yii::$app->request->cookies['language'];
        } else {
            Yii::$app->language = 'ru';
        }
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        // homePage Model 
        $homepage = new Home();
    
        return $this->render('index', compact([
            'hompage',
        ]));
    }


    /**
     * @param $lang
     * @return Response
     */
    public function actionSetLanguage($lang)
    {
     
        $langs = ['en', 'ru','uz'];
        if (in_array($lang, $langs)) {
            Yii::$app->language = $lang;
            Yii::$app->session->set('app_lang', $lang);
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'language',
                'value' => $lang,
            ]));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /** 
     * Feedback HEADER AJAX
     * @var Feedback()
     * @return bool
     */
    public function actionFeedback(){
        $model = new Feedback();
        $post = Yii::$app->request->post();

        if($model->getSaveFeedback($post)){
            return true;
        } else{
            return false;
        }

    }

    
    // ======================= ABOUT US  ========================
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $query      = AboutUsLeadership::find()->where(['status' => 1]);
        
        $page       = new Pagination([
            'totalCount' => $query->count(), 
            'pageSize' => 10
        ]);

        $leadership = $query->offset($page->offset)->limit($page->limit)->all();

        $structure  = AboutUsStructure::find()
            ->where(['id' => 1])
            ->one();

        return $this->render('about-us/about', compact([
            'page',
            'leadership',
            'structure',
        ]));
    }

    /**
     * @param $id
     * @return string
     */
    public function actionAboutUsLeadership($id){

        $leader = AboutUsLeadership::findOne($id);
        return $this->render('about-us/leadership', compact([
            'leader',
        ]));

    }

    //************************* END ABOUT **************************


    // =========================== NEWS  ==============================                       /// ADD COMMENT DON'T COMPLETE

    /**
     * @return string
     */
    public function actionNews(){

        // Last News
        $last = News::find()->where(['status' => 1])->orderBy(['id' => SORT_DESC])->limit(1)->one();

        // News with Pagination
        $query = News::find()
            ->filterWhere(['<>', 'id' ,$last->id])
            ->andFilterWhere(['status' => 1])
            ->orderBy(['id' => SORT_DESC]);
        $page = new Pagination(['totalCount' => $query->count(), 'pageSize' => 4]);
        $news = $query->offset($page->offset)->limit($page->limit)->all();

        // Search
        if(Yii::$app->request->get('q')){
            $q = Yii::$app->request->get('q');
            $query = News::find()
                ->filterWhere(['status' => 1])
                ->andFilterWhere(['like', 'title_'.Yii::$app->language,$q]);
            $page = new Pagination(['totalCount' => $query->count(), 'pageSize' => 4]);
            $news = $query->offset($page->offset)->limit($page->limit)->all();
            return $this->render('news/news-search',compact([
                'page',
                'news',
                'q'
            ]));
        }


        return $this->render('news/news',compact([
            'news',
            'page',
            'last'
        ]));

    }

    /**
     * @param $id
     * @return bool|string
     */
    public function actionNewsLike($id){
        if(Yii::$app->user->isGuest){
            return false;
        } else {
            $like = new NewsLikes();
            if($like->getChangeLike($id)){
                $item = News::findOne($id);
                return $this->renderAjax('news/like-ajax',['item' => $item]);
            }
        }


        return false;
    }

    //************************* END NEWS **************************

    // =========================== MEDIA  ==============================

    /**
     * @return string
     */
    public function actionAlbum(){
        $albums = Album::find()
            ->where(['status' => 1])
            ->limit(9)
            ->all();
        $albumsOther = Album::find()
            ->where(['status' => 1])
            ->offset(9)
            ->all();

        return $this->render('media/album',compact([
            'albums',
            'albumsOther',
        ]));
    }

    /**
     * @param $id
     * @return string
     */
    public function actionImages($id){
        $album = Album::findOne($id);

        return $this->render('media/images',compact([
            'album'
        ]));
    }

    /**
     * @return string
     */
    public function actionVideo(){
        $video = Video::find()->where(['status' => 1])->limit(9)->all();
        $videoOther = Video::find()->where(['status' => 1])->offset(9)->all();

        return $this->render('media/video',compact([
            'video',
            'videoOther',
        ]));

    }

    //************************* END MEDIA **************************


    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $this->layout = false;
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }

}
