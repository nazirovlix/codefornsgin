<?php
/**
 * User: NazirovLix
 * Date: 13.08.2018
 * Time: 17:58
 */

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\AboutUsAgancy;
use app\models\AboutUsLeadership;
use app\models\AboutUsStructure;
use app\models\Carousel;
use app\models\Country;
use app\models\forms\Login;
use app\models\Map;
use app\models\News;
use app\models\Photos;
use app\models\Section;
use app\models\forms\ContactForm;


class Home extends \yii\db\ActiveRecord
{

    /**
     * Home Page Carousel
     * @return array|\yii\db\ActiveRecord[]
     */

    public function getCarousel(){
      $item = Carousel::find()
          ->where(['status' => 1])
          ->all();
      return $item;
    }

    /**
     * Sections
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getSections(){
        $item = Section::find()
            ->where(['status' => 1])
            ->all();

        return $item;
    }

    /**
     * Google Map
     * Map in Home Page
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getMap(){
        $item = AboutUsAgancy::find()
            ->leftJoin('country','country.id = country_id')
            ->select([
                'about_us_agancy.id',
                'about_us_agancy.lat',
                'about_us_agancy.lng',
                'country.path',
                'country.name_'.Yii::$app->language,
                'country.city_'.Yii::$app->language
            ])
            ->where(['about_us_agancy.status' => 1])
            ->asArray()
            ->all();
        return $item;
    }

    /**
     * Home News -  limit(4)
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getNews(){
        $item = News::find()
            ->where(['status'=> 1])
            ->limit(4)
            ->orderBy(['id' => SORT_DESC])
            ->all();
            return $item;
    }

    /**
     * Album
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getImages(){
        $images =  AlbumImages::find()
            ->orderBy(new Expression('rand()'))
            ->limit(8)
            ->all();
        return $images;
    }

    /**
     * Video
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getVideo()
    {
        $video = Video::find()
            ->orderBy(new Expression('rand()'))
            ->limit(8)
            ->all();

         return $video;

    }

    /**
     * Home Page Events
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getEvents()
    {

        $events = Events::find()
            ->where(['status' => 1])
            ->limit(6)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        return $events;
    }


}